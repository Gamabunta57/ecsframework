using System;
using Triiinity.ECS.Entities;

namespace Triiinity.ECS.Components
{
    public sealed class ComponentHandler<T> where T : struct
    {
        public event EventHandler AddComponentEvent;
        public event EventHandler EnableComponentEvent;
        public event EventHandler DisableComponentEvent;

        public static Type ComponentKey;

        public T[] entityComponentList;

        public ComponentHandler(Type ComponentKeyHolder)
        {
            ComponentKey = ComponentKeyHolder;
        }

        public void Init(int numberOfEntity)
        {
            entityComponentList = new T[numberOfEntity];
        }

        public ref T GetComponent(ref Entity entity)
        {
            return ref entityComponentList[entity.Id];
        }

        public ref T GetComponent(int Id)
        {
            return ref entityComponentList[Id];
        }

        public void DisableComponent(ref Entity entity, int componentID)
        {
            entity.Mask &= ~componentID;
            NotifyDisableComponent(ref entity);
        }

        public void DisableComponent<ComponentType>(ref Entity entity)
        {
            var ID = (int)ComponentKey.GetField(typeof(ComponentType).Name).GetValue(null);
            entity.Mask &= ~ID;
            NotifyDisableComponent(ref entity);
        }

        public void EnableComponent(ref Entity entity, int componentID)
        {
            entity.Mask |= componentID;
            NotifyEnableComponent(ref entity);
        }

        public void EnableComponent<ComponentType>(ref Entity entity)
        {
            var ID = (int)ComponentKey.GetField(typeof(ComponentType).Name).GetValue(null);
            entity.Mask |= ID;
            NotifyEnableComponent(ref entity);
        }

        public void EnableMultipleComponent(ref Entity entity, params Type[] typeList)
        {
            for (int i = 0; i < typeList.Length; i++)
            {
                var ID = (int)ComponentKey.GetField(typeList[i].Name).GetValue(null);
                entity.Mask |= ID;
            }

            NotifyEnableComponent(ref entity);
        }

        public void NotifyAddComponent(ref Entity e)
        {
            AddComponentEvent?.Invoke(e, EventArgs.Empty);
        }

        public void NotifyDisableComponent(ref Entity e)
        {
            DisableComponentEvent?.Invoke(e, EventArgs.Empty);
        }

        public void NotifyEnableComponent(ref Entity e)
        {
            EnableComponentEvent?.Invoke(e, EventArgs.Empty);
        }
    }
}