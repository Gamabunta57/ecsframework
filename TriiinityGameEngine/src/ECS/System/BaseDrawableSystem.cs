﻿using Triiinity.ECS.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Triiinity.ECS.System
{
    public abstract class BaseDrawableSystem<T> : BaseSystem<T> where T : struct
    {
        public BaseDrawableSystem(ComponentHandler<T> cmpHandler) : base(cmpHandler) { }
        public virtual void Draw(SpriteBatch spriteBatch) { }
    }
}
