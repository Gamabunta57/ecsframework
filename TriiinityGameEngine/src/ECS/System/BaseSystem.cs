﻿using System;
using System.Collections.Generic;
using Triiinity.ECS.Components;
using Triiinity.ECS.Entities;

namespace Triiinity.ECS.System
{
    public abstract class BaseSystem<T> where T : struct
    {
        private readonly long keyComponent;
        protected readonly List<Entity> entityList = new List<Entity>();
        protected ComponentHandler<T> ComponentHandler;

        public BaseSystem(ComponentHandler<T> cmpHandler)
        {
            ComponentHandler = cmpHandler;
            ComponentHandler.AddComponentEvent += OnAddComponent;
            ComponentHandler.DisableComponentEvent += OnDisableComponent;
            ComponentHandler.EnableComponentEvent += OnAddComponent;
            //EntityHandler.RemoveEntityEvent += OnRemoveEntity;
            keyComponent = GetComponentKey();
        }

        protected void OnAddComponent(object sender, EventArgs args)
        {
            Entity e = (Entity)sender;
            if ((e.Mask & keyComponent) == keyComponent && entityList.IndexOf(e) == -1)
            {
                entityList.Add(e);
            }
        }

        protected void OnDisableComponent(object sender, EventArgs args)
        {
            Entity e = (Entity)sender;
            if ((e.Mask & keyComponent) != keyComponent && entityList.IndexOf(e) > -1)
            {
                entityList.Remove(e);
            }
        }

        protected void OnRemoveEntity(ref Entity entity)
        {
            if (entityList.IndexOf(entity) > -1)
            {
                entityList.Remove(entity);
            }
        }

        abstract protected long GetComponentKey();
    }
}
