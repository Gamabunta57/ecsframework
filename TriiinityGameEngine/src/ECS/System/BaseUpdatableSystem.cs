﻿using Microsoft.Xna.Framework;
using Triiinity.ECS.Components;

namespace Triiinity.ECS.System
{
    public abstract class BaseUpdatableSystem<T> : BaseSystem<T> where T : struct
    {
        public BaseUpdatableSystem(ComponentHandler<T> cmpHandler) : base(cmpHandler) { }

        public virtual void Update(GameTime gameTime) { }
    }
}
