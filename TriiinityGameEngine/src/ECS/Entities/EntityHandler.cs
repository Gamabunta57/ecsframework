namespace Triiinity.ECS.Entities
{
    public static class EntityHandler
    {
        static ushort _nextId = 0;
        //private static List<ushort> removedEntity;
        //public static event OnRemoveEntityEvent RemoveEntityEvent;

        //public delegate void OnRemoveEntityEvent(ref Entity entity);

        public static Entity GenerateEntity()
        {
            return new Entity
            {
                Id = _nextId++,
                Mask = 0x0
            };
        }

        //public static void RemoveEntity(ref Entity entity)
        //{
        //    removedEntity.Add(entity.Id);
        //    RemoveEntityEvent?.Invoke(ref entity);
        //}
    }
}