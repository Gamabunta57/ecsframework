namespace Triiinity.ECS.Entities
{
    public sealed class Entity
    {
        public ushort Id;
        public int Mask;
    }
}