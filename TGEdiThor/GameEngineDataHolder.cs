﻿using System.IO;
using ADreamWorld.WorldGenerator.DataStructure;
using Newtonsoft.Json;

namespace TGEdiThor
{
    public class GameEngineDataHolder
    {
        public static WorldGeneratorData GeneratorData => Instance.generatorData;
        public static void Reset() => _instance = new GameEngineDataHolder();

        public static void Open(string filePath)
        {
            using (var file = File.OpenRead(filePath))
            using(StreamReader reader = new StreamReader(file))
            {
                Instance.generatorData = JsonConvert.DeserializeObject<WorldGeneratorData>(reader.ReadToEnd());
            }
        }


        private static GameEngineDataHolder _instance;
        public static GameEngineDataHolder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameEngineDataHolder();
                }

                return _instance;
            }
        }

        private GameEngineDataHolder() { }

        public WorldGeneratorData generatorData;
    }
}