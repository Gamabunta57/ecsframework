﻿using System.Drawing;
using System.Globalization;
using Color = Microsoft.Xna.Framework.Color;

namespace TGEdiThor.TiledManager
{
    class TiledMap
    {
        public Color bgColor;
        public uint height;
        public uint width;
        public uint tileHeight;
        public uint tileWidth;

        public TiledLayer[] layers;
        public string type;

        public string backgroundColor
        {
            get
            {
                var converter = new ColorConverter();
                return converter.ConvertToString(bgColor);
            }
            set
            {
                var converter = new ColorConverter();
                var intVal = int.Parse(value.Remove(0, 1), NumberStyles.HexNumber);

                var baseColor = (System.Drawing.Color)converter.ConvertFromString(intVal.ToString());
                bgColor.A = value.Length == 7 ? (byte)255 : baseColor.A;
                bgColor.R = baseColor.R;
                bgColor.G = baseColor.G;
                bgColor.B = baseColor.B;
            }
        }
    }
}
