﻿namespace TGEdiThor.TiledManager
{
    public struct TiledLayer
    {
        public int id;
        public uint widht;
        public uint height;
        public string name;
        public TiledLayerType type;
        public bool visible;
        public int[] data;
        public TiledObject[] objects;
    }
}