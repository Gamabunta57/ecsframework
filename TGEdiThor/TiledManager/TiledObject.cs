﻿namespace TGEdiThor.TiledManager
{
    public struct TiledObject
    {
        public int gid;
        public int id;
        public uint width;
        public uint height;
        public string name;
        public string type;
        public bool visible;
        public float rotation;
        public int x;
        public int y;
    }
}