﻿using System;

namespace TGEdiThor
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new TGEdiThorEntryPoint())
            {
                game.Run();
            }
        }
    }
}
