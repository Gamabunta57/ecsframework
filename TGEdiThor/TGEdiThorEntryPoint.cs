﻿using ADreamWorld;
using ImGuiNET;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using TGEdiThor.ImGuiWindowElement;
using TGEdiThor.TilesetHandler;
using GameWindow = TGEdiThor.ImGuiWindowElement.GameWindow;
using Num = System.Numerics;
using System.IO;

namespace TGEdiThor
{
    class TGEdiThorEntryPoint : ADreamWorldGame
    {
        private static ImGuiRenderer _imGuiRenderer;
        private static TGEdiThorEntryPoint Instance;
        private List<ImGUIWindow> windowList = new List<ImGUIWindow>();
        private List<IAfterLoop> windowToProcessAfterLoop = new List<IAfterLoop>();
        private bool showEditor = true;
        private bool showDemo = true;
        private KeyboardState kbState;
        private bool pauseMainGame = true;
        //private const string TileSetFolder = @"E:\MonogameProject\Assets\Tileset";
        private const string TileSetFolder = @"./tilesets";

        public TGEdiThorEntryPoint() : base()
        {
            Instance = this;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferMultiSampling = true;
            SetNoRenderingInWindow();
            IsMouseVisible = true;
        }

        public static ImGuiRenderer GetRenderer()
        {
            return _imGuiRenderer;
        }

        public static GraphicsDevice GraphicsDeviceRef => Instance.GraphicsDevice;

        protected override void Initialize()
        {
            InitFolders();
            _imGuiRenderer = new ImGuiRenderer(this);
            _imGuiRenderer.RebuildFontAtlas();


            base.Initialize();

            var importTileset = new ImportTilesetWindow();
            var windowHolder = new TabWindow();
            var roomCreatorWindox = new RoomCreatorWindow(new TilesetHolder(TileSetFolder, _imGuiRenderer), importTileset);
            windowHolder.windowedTabList.Add(new WorldGeneratorWindow());
            windowHolder.windowedTabList.Add(new PerfWindow());
            windowHolder.windowedTabList.Add(roomCreatorWindox);
            windowHolder.windowedTabList.Add(new GameWindow(this, _imGuiRenderer));
            windowHolder.SetWindowSize(Window.ClientBounds.Width, Window.ClientBounds.Height);


            windowList.Add(FileBrowserWindow.Instance);
            windowList.Add(importTileset);
            windowList.Add(new MainMenuElement());
            windowList.Add(windowHolder);

            windowToProcessAfterLoop.Add(roomCreatorWindox);

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += OnWindowSize;
            Window.ClientSizeChanged += (object sender, EventArgs args) =>
            {
                windowHolder.SetWindowSize(Window.ClientBounds.Width, Window.ClientBounds.Height);
            };
            SetDearImGuiWindowStyle();
        }

        void SetDearImGuiWindowStyle()
        {
            var style = ImGui.GetStyle();
            style.Colors[(int)ImGuiCol.Text] = new Num.Vector4(1.00f, 1.00f, 1.00f, 1.00f);
            style.Colors[(int)ImGuiCol.TextDisabled] = new Num.Vector4(1.00f, 1.00f, 1.00f, 0.49f);
            style.Colors[(int)ImGuiCol.WindowBg] = new Num.Vector4(0.05f, 0.02f, 0.13f, 0.94f);
            style.Colors[(int)ImGuiCol.ChildBg] = new Num.Vector4(0.05f, 0.02f, 0.13f, 0.94f);
            style.Colors[(int)ImGuiCol.PopupBg] = new Num.Vector4(0.00f, 0.00f, 0.00f, 0.95f);
            style.Colors[(int)ImGuiCol.Border] = new Num.Vector4(0.21f, 0.04f, 0.74f, 1.00f);
            style.Colors[(int)ImGuiCol.FrameBg] = new Num.Vector4(0.14f, 0.09f, 0.31f, 0.79f);
            style.Colors[(int)ImGuiCol.FrameBgHovered] = new Num.Vector4(0.21f, 0.13f, 0.44f, 1.00f);
            style.Colors[(int)ImGuiCol.FrameBgActive] = new Num.Vector4(0.29f, 0.19f, 0.61f, 1.00f);
            style.Colors[(int)ImGuiCol.TitleBg] = new Num.Vector4(0.11f, 0.00f, 0.44f, 0.94f);
            style.Colors[(int)ImGuiCol.TitleBgActive] = new Num.Vector4(0.24f, 0.00f, 1.00f, 0.94f);
            style.Colors[(int)ImGuiCol.TitleBgCollapsed] = new Num.Vector4(0.04f, 0.00f, 0.17f, 0.76f);
            style.Colors[(int)ImGuiCol.MenuBarBg] = new Num.Vector4(0.00f, 0.00f, 0.00f, 1.00f);
            style.Colors[(int)ImGuiCol.ScrollbarBg] = new Num.Vector4(0.02f, 0.02f, 0.02f, 1.00f);
            style.Colors[(int)ImGuiCol.ScrollbarGrab] = new Num.Vector4(0.00f, 0.53f, 0.22f, 1.00f);
            style.Colors[(int)ImGuiCol.ScrollbarGrabHovered] = new Num.Vector4(0.01f, 0.84f, 0.36f, 1.00f);
            style.Colors[(int)ImGuiCol.ScrollbarGrabActive] = new Num.Vector4(0.00f, 1.00f, 0.41f, 1.00f);
            style.Colors[(int)ImGuiCol.CheckMark] = new Num.Vector4(0.00f, 1.00f, 0.41f, 1.00f);
            style.Colors[(int)ImGuiCol.SliderGrab] = new Num.Vector4(0.00f, 0.53f, 0.22f, 1.00f);
            style.Colors[(int)ImGuiCol.SliderGrabActive] = new Num.Vector4(0.00f, 0.83f, 0.34f, 1.00f);
            style.Colors[(int)ImGuiCol.Button] = new Num.Vector4(0.00f, 0.53f, 0.22f, 1.00f);
            style.Colors[(int)ImGuiCol.ButtonHovered] = new Num.Vector4(0.00f, 0.70f, 0.29f, 1.00f);
            style.Colors[(int)ImGuiCol.ButtonActive] = new Num.Vector4(0.00f, 1.00f, 0.41f, 1.00f);
            style.Colors[(int)ImGuiCol.Header] = new Num.Vector4(0.00f, 0.53f, 0.22f, 0.56f);
            style.Colors[(int)ImGuiCol.HeaderHovered] = new Num.Vector4(0.00f, 0.53f, 0.22f, 0.72f);
            style.Colors[(int)ImGuiCol.HeaderActive] = new Num.Vector4(0.00f, 0.79f, 0.33f, 0.86f);
            style.Colors[(int)ImGuiCol.Separator] = new Num.Vector4(0.00f, 0.53f, 0.22f, 0.45f);
            style.Colors[(int)ImGuiCol.SeparatorHovered] = new Num.Vector4(0.00f, 0.53f, 0.22f, 0.45f);
            style.Colors[(int)ImGuiCol.SeparatorActive] = new Num.Vector4(0.00f, 0.53f, 0.22f, 0.45f);
            style.Colors[(int)ImGuiCol.ResizeGrip] = new Num.Vector4(0.00f, 0.53f, 0.22f, 1.00f);
            style.Colors[(int)ImGuiCol.ResizeGripHovered] = new Num.Vector4(0.00f, 0.70f, 0.29f, 1.00f);
            style.Colors[(int)ImGuiCol.ResizeGripActive] = new Num.Vector4(0.00f, 1.00f, 0.42f, 1.00f);
            style.Colors[(int)ImGuiCol.Tab] = new Num.Vector4(0.00f, 0.53f, 0.22f, 1.00f);
            style.Colors[(int)ImGuiCol.TabHovered] = new Num.Vector4(0.00f, 0.70f, 0.29f, 1.00f);
            style.Colors[(int)ImGuiCol.TabActive] = new Num.Vector4(0.00f, 1.00f, 0.42f, 1.00f);

            style.WindowPadding.X = 10;
            style.WindowPadding.Y = 10;

            style.PopupRounding = 0;

            style.FramePadding.X = 5;
            style.FramePadding.Y = 4;

            style.ItemSpacing.X = 10;
            style.ItemSpacing.Y = 5;

            style.ItemInnerSpacing.X = 3;
            style.ItemInnerSpacing.Y = 0;

            style.ScrollbarSize = 10;
            style.GrabMinSize = 10;

            style.WindowRounding = 0;
            style.ChildRounding = 0;
            style.ScrollbarRounding = 0;

            style.TabRounding = 0;
        }

        void OnWindowSize(object sender, EventArgs e)  
        {
            //windowHolder.SetWindowSize(Window.ClientBounds.Width, Window.ClientBounds.Height);
            graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
            graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!pauseMainGame)
            {
                base.Update(gameTime);
            }

            if (!kbState.IsKeyDown(Keys.F1) && Keyboard.GetState().IsKeyDown(Keys.F1))
            {
                showEditor = !showEditor;
            }
            if (!kbState.IsKeyDown(Keys.F2) && Keyboard.GetState().IsKeyDown(Keys.F2))
            {
                showDemo = !showDemo;
            }
            if (!kbState.IsKeyDown(Keys.P) && Keyboard.GetState().IsKeyDown(Keys.P))
            {
                pauseMainGame = !pauseMainGame;
            }

            kbState = Keyboard.GetState();
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            _imGuiRenderer.BeforeLayout(gameTime);

            // Draw our UI
            ImGuiLayout();

            // Call AfterLayout now to finish up and draw all the things
            _imGuiRenderer.AfterLayout();

            foreach (var window in windowToProcessAfterLoop)
                window.AfterLoop();

        }

        protected virtual void ImGuiLayout()
        {
            if (showEditor)
            {
                for (var i = 0; i < windowList.Count; i++)
                {
                    if (windowList[i].isOpen)
                    {
                        windowList[i].ImGUILayout();
                    }
                }
            }

            if (showDemo)
            {
                ImGui.SetNextWindowPos(new Num.Vector2(650, 20), ImGuiCond.FirstUseEver);
                ImGui.ShowDemoWindow();
            }
        }

        void InitFolders()
        {
            if (!Directory.Exists(TileSetFolder))
                Directory.CreateDirectory(TileSetFolder);
        }
    }
}
