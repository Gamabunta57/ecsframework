﻿using System;
using System.Numerics;
using ADreamWorld;
using ImGuiNET;

namespace TGEdiThor.ImGuiWindowElement
{
    class GameWindow : ImGUIWindow
    {
        public override string Name => "Game";

        IntPtr gameRenderTargetPtr;
        float ratio;

        public GameWindow(ADreamWorldGame game, ImGuiRenderer mainRenderer) : base()
        {
            gameRenderTargetPtr = mainRenderer.BindTexture(game.RenderTarget);
            ratio = (float)game.RenderTarget.Height / game.RenderTarget.Width;
            Console.WriteLine($"{game.RenderTarget.Height} x {game.RenderTarget.Width} / {ratio}");
        }

        protected override void ImGUIContent()
        {
            float width = ImGui.GetWindowContentRegionWidth();
            float height = width * ratio;
            float Yoffset = ImGui.GetWindowPos().Y + 20;
            if (height > ImGui.GetWindowHeight() - Yoffset)
            {
                height = ImGui.GetWindowHeight() - Yoffset;
                width = height / ratio;
            }

            ImGui.Image(gameRenderTargetPtr, new Vector2(width,height));
        }

    }
}
