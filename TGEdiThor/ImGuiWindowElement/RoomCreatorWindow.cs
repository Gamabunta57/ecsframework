﻿using ADreamWorld.WorldGenerator.DataStructure;
using ImGuiNET;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TGEdiThor.TiledManager;
using TGEdiThor.TilesetHandler;
using Num = System.Numerics;

namespace TGEdiThor.ImGuiWindowElement
{
    class RoomCreatorWindow : ImGUIWindow, IAfterLoop
    {
        public override string Name => "Room creator";

        bool isMapSet;
        int currentRoomSelected = -1;
        int currentHoveringRoom = -1;
        string tilesetSelected = "";
        TilesetHolder tilesetManager;
        Tileset tilesetToRemove;
        FileInfo currentFileInfo;
        ImportTilesetWindow importTileset;
        List<Room> roomList = new List<Room>();
        Num.Vector2 tilesetDisplaySize;
        TiledMap map;

        public RoomCreatorWindow(TilesetHolder tileset, ImportTilesetWindow importTilesetWindow)
        {
            tilesetManager = tileset;
            importTileset = importTilesetWindow;
        }

        protected override void ImGUIContent()
        {
            float width = ImGui.GetWindowContentRegionWidth();

            var flags = ImGuiWindowFlags.HorizontalScrollbar;
            ImGui.BeginChild("room_list", new Num.Vector2(width * .1f, -1), false, flags);
            SetRoomListWindow();
            ImGui.EndChild();

            ImGui.SameLine();

            ImGui.BeginChild("room_result", new Num.Vector2(width * .7f, -1), false, flags);
            SetGridWindow();
            ImGui.EndChild();

            ImGui.SameLine();

            ImGui.BeginChild("room_param", new Num.Vector2(width * .2f, -1), false, flags);
            SetParamWindow();
            ImGui.EndChild();

            if (null == currentFileInfo)
            {
                return;
            }

            var info = new FileInfo(currentFileInfo.FullName);
            if (info.LastWriteTime != currentFileInfo.LastWriteTime)
            {
                SetMap(currentFileInfo.FullName);
            }
        }

        void SetRoomListWindow()
        {
            var count = roomList.Count;
            var colors = ImGui.GetStyle();
            var indexToRemove = -1;
            var hovering = -1;

            if (ImGui.Button("Add new room"))
            {
                roomList.Add(new Room() { GridWidth = 3, GridHeight = 3, HookList = new List<Point>(), RoomSpaceList = new List<Point>() });
            }

            for (int i = 0; i < count; i++)
            {
                Num.Vector4 color =
                    i == currentRoomSelected ?
                    colors.Colors[(int)ImGuiCol.ButtonActive] :
                    i == currentHoveringRoom ?
                    colors.Colors[(int)ImGuiCol.Text] :
                    colors.Colors[(int)ImGuiCol.TextDisabled];

                ImGui.TextColored(color, $"room {i + 1}");
                if (ImGui.IsItemClicked())
                {
                    currentRoomSelected = i;
                }

                if (ImGui.IsItemHovered())
                {
                    hovering = i;
                }

                ImGui.SameLine();
                if (ImGui.Button($"-###Button{i}"))
                {
                    indexToRemove = i;
                }
            }

            currentHoveringRoom = hovering;

            if (indexToRemove > -1)
            {
                roomList.RemoveAt(indexToRemove);
                currentRoomSelected = Math.Min(currentRoomSelected, roomList.Count - 1);
            }
        }

        void SetParamWindow()
        {

            if (ImGui.BeginCombo("Tileset", tilesetSelected))
            {
                foreach (var tilesetName in tilesetManager.tilesetDictionary.Keys)
                {
                    var isSelected = (tilesetSelected == tilesetName);

                    if (ImGui.Selectable(tilesetName, isSelected))
                    {
                        tilesetSelected = tilesetName;
                    }

                    if (isSelected)
                    {
                        ImGui.SetItemDefaultFocus();
                    }
                }
                ImGui.EndCombo();
            }


            if (ImGui.Button("Import new tileset") && !importTileset.isOpen)
            {
                FileBrowserWindow.Open(TryImportFile, "*.png");
            }

            if (!string.IsNullOrWhiteSpace(tilesetSelected))
            {
                ImGui.SameLine();
                if (ImGui.Button("Delete tileset") && !importTileset.isOpen)
                {
                    tilesetToRemove = tilesetManager.tilesetDictionary[tilesetSelected];
                    tilesetSelected = null;
                }
            }


            if (!string.IsNullOrWhiteSpace(tilesetSelected))
            {
                var tileset = tilesetManager.tilesetDictionary[tilesetSelected];
                tilesetDisplaySize.X = ImGui.CalcItemWidth();
                tilesetDisplaySize.Y = tilesetDisplaySize.X * (tileset.tileTexture2D.Height / tileset.tileTexture2D.Width);
                ImGui.Image(tileset.RefTexture, tilesetDisplaySize);
            }
        }

        void TryImportFile(string filepath)
        {
            var tileset = tilesetManager.Add(filepath);
            importTileset.Open(tileset, ValidateImportTileset, CancelImportTileset);
        }

        void ValidateImportTileset(string tilesetName, Tileset tileset)
        {
            tilesetManager.Renamekey(tilesetName, tileset);
            tilesetManager.SaveTileset(tilesetName, tileset);
        }

        void CancelImportTileset(Tileset tileset)
        {
            tileset.UnlinkPath();
            tilesetToRemove = tileset;
        }

        public void AfterLoop()
        {
            if (null == tilesetToRemove)
            {
                return;
            }

            tilesetManager.Remove(tilesetToRemove);
            tilesetToRemove.Remove();
            tilesetToRemove = null;
        }

        void SetGridWindow()
        {
            if (ImGui.Button("Import tile map from tiled"))
            {
                FileBrowserWindow.Open(SetMap, "*.json");
            }

            if (!isMapSet)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(tilesetSelected))
            {
                return;
            }

            ImGui.BeginChild("map_child_preview",ImGui.GetWindowSize(),false);
            var offset = ImGui.GetWindowPos();
            //var offset = Num.Vector2.Zero;
            var rowLength = 16u;
            var drawList = ImGui.GetWindowDrawList();
            drawList.AddRectFilled(offset, new Num.Vector2(map.width * map.tileWidth, map.height * map.tileHeight) + offset, map.bgColor.PackedValue);
            var tileset = tilesetManager.tilesetDictionary[tilesetSelected];
            for (var i = 0u; i < map.layers.Length; i++)
            {
                if (map.layers[i].type == TiledLayerType.ObjectGroup)
                {
                    DisplayObjectLayer(ref drawList, ref i, ref rowLength, ref tileset, ref offset);
                }
                else
                {
                    DisplayTilemapLayer(ref drawList, ref i, ref rowLength, ref tileset, ref offset);
                }


            }

            /*
            for (var y = 0; y < map.height; y++)
            {
                for (var x = 0; x < map.width; x++)
                {
                    ImGui.Text(map.layers[0].data[x + y * map.width].ToString().PadLeft(2));
                    ImGui.SameLine();
                }
                ImGui.NewLine();
            }
            */

            ImGui.EndChild();
        }

        void DisplayTilemapLayer(ref ImDrawListPtr drawList, ref uint layerIndex, ref uint rowLength, ref Tileset tileset, ref Num.Vector2 offset)
        {
            for (var y = 0; y < map.height; y++)
            {
                var yOfffset = y * map.width;
                for (var x = 0; x < map.width; x++)
                {
                    var refTile = map.layers[layerIndex].data[x + yOfffset] - 1;
                    var refTileRow = refTile / rowLength;
                    var refTileCol = refTile - (refTileRow * rowLength);
                    drawList.AddImage(
                        tileset.RefTexture,
                        new Num.Vector2(x, y) * Tileset.CellSize + offset,
                        new Num.Vector2(x + 1, y + 1) * Tileset.CellSize + offset,
                        new Num.Vector2((refTileCol) / 16f, (refTileRow) / 16f),
                        new Num.Vector2((refTileCol + 1) / 16f, (refTileRow + 1) / 16f)
                    );
                    ImGui.SameLine();
                }
                ImGui.NewLine();
            }
        }

        void DisplayObjectLayer(ref ImDrawListPtr drawList, ref uint layerIndex, ref uint rowLength, ref Tileset tileset, ref Num.Vector2 offset)
        {
            for (var o = 0; o < map.layers[layerIndex].objects.Length; o++)
            {
                var refTile = map.layers[layerIndex].objects[o].gid - 1;
                var refTileRow = refTile / rowLength;
                var refTileCol = refTile - (refTileRow * rowLength);
                var width = map.layers[layerIndex].objects[o].width;
                var height = map.layers[layerIndex].objects[o].height;
                var x = map.layers[layerIndex].objects[o].x;
                var y = map.layers[layerIndex].objects[o].y - height;

                drawList.AddImage(
                    tileset.RefTexture,
                    new Num.Vector2(x, y) + offset,
                    new Num.Vector2(x + width, y + height) + offset,
                    new Num.Vector2((refTileCol) / 16f, (refTileRow) / 16f),
                    new Num.Vector2((refTileCol + 1) / 16f, (refTileRow + 1) / 16f)
                );
                ImGui.SameLine();
            }
            ImGui.NewLine();

        }

        void SetMap(string chosenFile)
        {
            try
            {
                using (var file = File.OpenText(chosenFile))
                {
                    map = JsonConvert.DeserializeObject<TiledMap>(file.ReadToEnd());
                }
                isMapSet = true;
                SetWatcherOnFile(chosenFile);
            }
            catch (Exception e)
            {
                isMapSet = false;
                Console.WriteLine(e.Message);
            }
        }

        void SetWatcherOnFile(string filePath)
        {
            currentFileInfo = new FileInfo(filePath);
        }

        /*
        void SetGridWindow()
        {
            var drawList = ImGui.GetWindowDrawList();
            var nbCell = 25;
            var size = new Num.Vector2(nbCell * Tileset.CellSize);
            var offset = ImGui.GetCursorScreenPos();
            var cursorPos = ImGui.GetIO().MousePos;
            for (var i = 0; i < nbCell +1; i++)
            {
                var point = i * Tileset.CellSize;
                drawList.AddLine(new Num.Vector2(point + offset.X, offset.Y), new Num.Vector2(point + offset.X, size.X + offset.Y),0x11FFFFFF);
                drawList.AddLine(new Num.Vector2(offset.X, point + offset.Y), new Num.Vector2(size.Y + offset.X, point + offset.Y), 0x11FFFFFF);
            }
            drawList.AddCircle(cursorPos, 5, 0xFF5500DD);
            var cellPos = new Num.Vector2((int)Math.Floor((cursorPos.X - offset.X) / Tileset.CellSize), (int)Math.Floor((cursorPos.Y - offset.Y) / Tileset.CellSize));
            var rectStart = cellPos * Tileset.CellSize + offset + new Num.Vector2(1);
            var rectSize = rectStart + new Num.Vector2(Tileset.CellSize -1);
            drawList.AddRect(rectSize,rectStart,0xff00ff00);
        }
        */

        /*
        void SetGridWindow()
        {
            if (currentRoomSelected < 0)
            {
                return;
            }

            Room room = roomList[currentRoomSelected];
            ImGui.Text($"Room {currentRoomSelected + 1}");

            var currentWidth = ImGui.CalcItemWidth();
            ImGui.PushItemWidth(currentWidth * .5f);
            ImGui.SliderInt("Grid width", ref room.GridWidth, 1, 10); ImGui.SameLine();
            ImGui.SliderInt("Grid height", ref room.GridHeight, 1, 10);

            ImGui.BeginGroup();
            if (ImGui.Button("Add room space"))
            {
                room.RoomSpaceList.Add(new Point(0));
            }

            ImGui.PushItemWidth(currentWidth * .25f);
            for (var i = 0; i < room.RoomSpaceList.Count; i++)
            {
                var point = room.RoomSpaceList[i];
                ImGui.SliderInt($"X###InputSpaceX{i}", ref point.X, 0, room.GridWidth - 1);
                ImGui.SameLine();
                ImGui.SliderInt($"Y###InputSpaceY{i}", ref point.Y, 0, room.GridHeight - 1);
                room.RoomSpaceList[i] = point;
            }
            ImGui.PopItemWidth();

            ImGui.EndGroup();

            ImGui.SameLine();

            ImGui.BeginGroup();
            if (ImGui.Button("Add room hook"))
            {
                room.HookList.Add(new Point(0));
            }

            ImGui.PushItemWidth(currentWidth * .25f);
            for (var i = 0; i < room.HookList.Count; i++)
            {
                var point = room.HookList[i];
                ImGui.SliderInt($"X###InputHookX{i}", ref point.X, 0, room.GridWidth - 1);
                ImGui.SameLine();
                ImGui.SliderInt($"Y###InputHookY{i}", ref point.Y, 0, room.GridHeight - 1);
                room.HookList[i] = point;
            }
            ImGui.EndGroup();

            ImGui.PopItemWidth();
            ImGui.PopItemWidth();

            roomList[currentRoomSelected] = room;
        }
        */
    }
}