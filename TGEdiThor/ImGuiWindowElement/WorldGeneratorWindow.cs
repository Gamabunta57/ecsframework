﻿using ADreamWorld.WorldGenerator.DataStructure;
using ImGuiNET;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using Num = System.Numerics;

namespace TGEdiThor.ImGuiWindowElement
{
    class WorldGeneratorWindow : ImGUIWindow
    {
        public override string Name => "World Generator";

        private Random rand = new Random();
        private readonly IntPtr image;
        private WorldGeneratorData data;

        protected override void ImGUIContent()
        {
            data = GameEngineDataHolder.GeneratorData;
            float width = ImGui.GetWindowContentRegionWidth();

            var flags = ImGuiWindowFlags.HorizontalScrollbar;
            ImGui.BeginChild("world_param", new Num.Vector2(width * .3f, -1), false, flags);
            SetParamWindow();
            ImGui.EndChild();

            ImGui.SameLine();

            ImGui.BeginChild("world_result", new Num.Vector2(width * .7f, -1), false, flags);
            SetResultWindow();
            ImGui.EndChild();
        }

        void SetParamWindow()
        {
            RNGPart();
            GridPart();
            StepPart();
        }

        void SetResultWindow()
        {
            if (null == image)
            {
                return;
            }
            ImGui.Image(image, -Num.Vector2.One);
        }

        void RNGPart()
        {
            if (!ImGui.CollapsingHeader("RNG") || null == data)
            {
                return;
            }

            ImGui.Checkbox("Random seed ?", ref data.UseRandomSeed);
            ImGui.InputInt("Manual seed", ref data.Seed, 1, 10, data.UseRandomSeed ? ImGuiInputTextFlags.ReadOnly : ImGuiInputTextFlags.None);

            if (ImGui.Button("Regenerate Seed") && data.UseRandomSeed)
            {
                data.Seed = rand.Next();
            }
        }

        void GridPart()
        {
            if (!ImGui.CollapsingHeader("Grid") || null == data)
            {
                return;
            }

            var width = ImGui.CalcItemWidth();
            ImGui.BeginGroup();
            ImGui.PushItemWidth(80);
            ImGui.InputInt("GridWidth", ref data.GridWidth); ImGui.SameLine();
            ImGui.InputInt("GridHeight", ref data.GridHeight);
            ImGui.EndGroup();
            ImGui.PushItemWidth(width);

            string[] origins = WorldGeneratorData.AvailableOrigins;

            ImGui.Combo("Origin type", ref data.OriginTypeIndex, origins, origins.Length);
        }

        void StepPart()
        {
            var style = ImGui.GetStyle();
            if (!ImGui.CollapsingHeader("Step List") || null == data)
            {
                return;
            }

            string[] stackOptionList = GeneratorStep.AvailableStackOption;
            var countStackOption = stackOptionList.Length;

            string[] abilityList = GeneratorStep.AvailableAbility;
            var countAbility = abilityList.Length;

            if (null == data.StepList)
            {
                return;
            }

            for (var i = 0; i < data.StepList.Count; i++)
            {
                ImGui.Spacing();
                ImGui.Spacing();
                if (!ImGui.CollapsingHeader($"Step {i}"))
                {
                    ImGui.Spacing();
                    ImGui.Spacing();
                    ImGui.Separator();
                    continue;
                }
                GeneratorStep step = data.StepList[i];

                var width = ImGui.CalcItemWidth();
                ImGui.PushItemWidth(80);
                ImGui.InputInt($"Nb room###step{i}", ref step.RoomCountToGenerate, 1, 10); ImGui.SameLine();
                ImGui.InputInt($"Nb bis###step{i}", ref step.RoomCountToGenerateBis, 1, 10);
                ImGui.PushItemWidth(width);
                ImGui.Combo($"Ability###step{i}", ref step.CurrentAbilityIndex, abilityList, countAbility);
                ImGui.Combo($"Stack option###step{i}", ref step.StackOptionIndex, stackOptionList, countStackOption);

                for (var k = 0; k < step.TemplateRoomList.Count; k++)
                {
                    Room room = step.TemplateRoomList[k];
                    if (ImGui.CollapsingHeader($"Room###{i}{k}"))
                    {
                        ImGui.Text("Space");
                        for (var j = 0; j < room.RoomSpaceList.Count; j++)
                        {
                            Point vec = room.RoomSpaceList[j];
                            ImGui.InputInt($"###roomSpaceX{i}-{j}-{k}", ref vec.X); ImGui.SameLine(0, 10);
                            ImGui.InputInt($"###roomSpaceY{i}-{j}-{k}", ref vec.Y);
                            room.RoomSpaceList[j] = vec;
                        }
                        ImGui.Text("Hook");
                        for (var j = 0; j < room.HookList.Count; j++)
                        {
                            Point vec = room.HookList[j];
                            ImGui.InputInt($"###roomHookX{i}-{j}-{k}", ref vec.X); ImGui.SameLine(0, 10);
                            ImGui.InputInt($"###roomHookY{i}-{j}-{k}", ref vec.Y);
                            room.HookList[j] = vec;
                        }
                        step.TemplateRoomList[k] = room;
                    }
                }
                data.StepList[i] = step;
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Separator();
            }
        }
    }
}
