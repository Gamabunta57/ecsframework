﻿using System;
using System.Numerics;
using ADreamWorld.WorldGenerator.DataStructure;
using ImGuiNET;

namespace TGEdiThor.ImGuiWindowElement
{
    class MainMenuElement : ImGUIWindow
    {

        public override void ImGUILayout()
        {
            ImGUIContent();
        }

        protected override void ImGUIContent()
        {
            if (!ImGui.BeginMainMenuBar())
            {
                return;
            }

            if (ImGui.BeginMenu("File"))
            {
                if (ImGui.MenuItem("New"))
                {
                    GameEngineDataHolder.Reset();
                }

                if (ImGui.MenuItem("Open"))
                {
                    FileBrowserWindow.Open(SetDataWithJsonfile, "*.json");
                }

                if (ImGui.MenuItem("Save"))
                {

                }

                if (ImGui.MenuItem("Save as"))
                {

                }


                ImGui.EndMenu();
            }
            ImGui.EndMainMenuBar();
        }

        void SetDataWithJsonfile(string jsonFilePath)
        {
            Console.WriteLine($"File selected : {jsonFilePath}");
            GameEngineDataHolder.Open(jsonFilePath);
        }

    }
}