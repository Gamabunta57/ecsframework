﻿using System;
namespace TGEdiThor.ImGuiWindowElement
{
    public interface IAfterLoop
    {
        void AfterLoop();
    }
}
