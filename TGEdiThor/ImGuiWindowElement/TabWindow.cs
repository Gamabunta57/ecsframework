﻿using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace TGEdiThor.ImGuiWindowElement
{
    class TabWindow : ImGUIWindow
    {
        public List<ImGUIWindow> windowedTabList = new List<ImGUIWindow>();

        Vector2 windowSize = new Vector2(-1,-1);
        Vector2 windowPos = Vector2.Zero;
        readonly float _mainMenuHeight = 20;

        public override void ImGUILayout()
        {
            ImGui.SetNextWindowPos(windowPos, ImGuiCond.Always);
            ImGui.SetNextWindowSize(windowSize, ImGuiCond.Always);

            ImGui.Begin(Name, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoMove);
            ImGUIContent();
            ImGui.End();
        }

        protected override void ImGUIContent()
        {
            if (!ImGui.BeginTabBar("main_tab_bar"))
            {
                return;
            }

            for (var i = 0; i < windowedTabList.Count; i++)
            {
                if (ImGui.BeginTabItem(windowedTabList[i].Name))
                {
                    windowedTabList[i].ImGUITabLayout();
                    ImGui.EndTabItem();
                }
            }
            ImGui.EndTabBar();
        }

        public void SetWindowSize(int width, int height)
        {
            windowSize.X = width;
            windowSize.Y = height - _mainMenuHeight;
            windowPos.X = 0;
            windowPos.Y = _mainMenuHeight;
        }

        internal void OnClientSizeChanged(object sender, EventArgs e)
        {
            Console.WriteLine(sender);
        }
    }
}
