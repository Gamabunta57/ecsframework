﻿using ImGuiNET;

namespace TGEdiThor.ImGuiWindowElement
{
    abstract class ImGUIWindow
    {
        public virtual string Name => "Standard window";
        public bool isOpen = true;

        public virtual void ImGUILayout()
        {
            ImGui.Begin(Name, ImGuiWindowFlags.None);
            ImGUIContent();
            ImGui.End();
        }

        public void ImGUITabLayout()
        {
            ImGUIContent();
        }

        abstract protected void ImGUIContent();
    }
}
