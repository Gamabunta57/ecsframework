﻿using ImGuiNET;

namespace TGEdiThor.ImGuiWindowElement
{
    class PerfWindow : ImGUIWindow
    {
        public override string Name => "Perf";

        protected override void ImGUIContent()
        {
            ImGui.Text(string.Format("{0:F3} ms/f", 1000f / ImGui.GetIO().Framerate));
            ImGui.Text(string.Format("{0:F1} FPS", ImGui.GetIO().Framerate));
        }
    }
}
