﻿using ImGuiNET;
using System;
using System.ComponentModel;
using System.IO;
using System.Numerics;

namespace TGEdiThor.ImGuiWindowElement
{
    class FileBrowserWindow : ImGUIWindow
    {
        public override string Name => "File Browser";

        static FileBrowserWindow _instance;
        public static FileBrowserWindow Instance
        {
            get
            {
                if(null == _instance)
                    _instance = new FileBrowserWindow();
                return _instance;
            }
        }

        string currentPath;
        string lastPath;
        string fileFilter = "*";
        readonly string defaultFolder;
        bool showingDrives;
        FileInfo[] fileList;
        DirectoryInfo[] directoryList;
        Vector2 windowSize = new Vector2(500, 440);
        Vector2 childrenSize = new Vector2(240, 340);
        ImGuiStylePtr colors = ImGui.GetStyle();
        Action<string> fileSelectedCallback;

        bool _fileSelected;
        string _fileChosen;
        string FileChosen
        {
            get
            {
                var path = _fileChosen;
                FileChosen = null;
                return Path.Combine(currentPath, path);
            }
            set
            {
                _fileSelected = value != null;
                _fileChosen = value;
            }
        }

        private FileBrowserWindow() : base()
        {
            defaultFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ResetPath();
            isOpen = false;
            colors = ImGui.GetStyle();
        }

        public override void ImGUILayout()
        {
            ImGui.SetNextWindowSize(windowSize, ImGuiCond.Always);
            ImGui.Begin(Name, ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoResize);
            ImGUIContent();
            ImGui.End();
        }

        protected override void ImGUIContent()
        {

            ImGui.InputText("Current Path", ref currentPath, 200);
            if (!Directory.Exists(currentPath))
            {
                ResetPath();
            }
            else if (currentPath != lastPath)
            {
                SetNewPath(null);
            }

            ImGui.BeginChild("directory_part", childrenSize);
            if (!showingDrives)
            {
                ImGui.Text("../");
                if (ImGui.IsItemClicked())
                {
                    GoToParentFolder();
                }
            }

            foreach (var dir in directoryList)
            {
                ImGui.Text($"{dir.Name}");
                if (ImGui.IsItemClicked())
                {
                    SetNewPath(dir.Name);
                }
            }
            ImGui.EndChild();
            ImGui.SameLine();
            ImGui.BeginChild("file_part", childrenSize);
            foreach (var file in fileList)
            {
                ImGui.TextColored(file.Name == _fileChosen ? colors.Colors[(int)ImGuiCol.ButtonActive] : colors.Colors[(int)ImGuiCol.Text], file.Name);
                if (ImGui.IsItemClicked())
                {
                    if (_fileChosen == file.Name)
                    {
                        fileSelectedCallback?.Invoke(FileChosen);
                        isOpen = false;
                        return;
                    }
                    else
                    {
                        FileChosen = file.Name;
                    }
                }
            }
            ImGui.EndChild();

            if (_fileSelected)
            {
                if (ImGui.Button("Select"))
                {
                    fileSelectedCallback?.Invoke(FileChosen);
                    isOpen = false;
                    return;
                }
                ImGui.SameLine();
            }

            if (ImGui.Button("Cancel"))
            {
                isOpen = false;
            }
        }

        void OpenFileBrowser(Action<string> fileSelectedCallback, string filter = "*")
        {
            this.fileSelectedCallback = fileSelectedCallback;
            fileFilter = filter;
            InitPathData();
            isOpen = true;
            ImGui.SetWindowFocus(Name);
        }

        public static void Open(Action<string> fileSelectedCallback, string filter = "*")
        {
            Instance.OpenFileBrowser(fileSelectedCallback, filter);
        }

        void SetNewPath(string newPathPart)
        {
            FileChosen = null;
            var tempPath = currentPath;
            try
            {
                if (null != newPathPart)
                {
                    currentPath = Path.Combine(currentPath, newPathPart);
                }

                InitPathData();

                showingDrives = false;
            }
            catch (Exception)
            {
                currentPath = tempPath;
            }
        }

        void ResetPath()
        {
            currentPath = defaultFolder;
            InitPathData();
        }

        void InitPathData()
        {
            var directory = new DirectoryInfo(currentPath);
            currentPath = directory.FullName;
            directoryList = directory.GetDirectories();
            fileList = directory.GetFiles(fileFilter);
            lastPath = currentPath;
        }

        void GoToParentFolder()
        {
            FileChosen = null;
            var info = new DirectoryInfo(currentPath);
            showingDrives = info.Root.Name == info.Name;

            if (!showingDrives)
            {
                SetNewPath("..");
                return;
            }

            var driveList = Directory.GetLogicalDrives();
            directoryList = new DirectoryInfo[driveList.Length];
            fileList = new FileInfo[0];

            for (var i = 0; i < driveList.Length; i++)
            {
                directoryList[i] = new DirectoryInfo($"{driveList[i]}");
            }
        }
    }
}