﻿
using ImGuiNET;
using System;
using System.Numerics;
using TGEdiThor.TilesetHandler;

namespace TGEdiThor.ImGuiWindowElement
{
    class ImportTilesetWindow : ImGUIWindow
    {
        public override string Name => "Import tileset";

        Tileset currentTileset;
        string tilesetName = "";

        Action<string, Tileset> validateCallback;
        Action<Tileset> cancelCallback;

        public ImportTilesetWindow() : base()
        {
            isOpen = false;
        }

        public override void ImGUILayout()
        {
            ImGui.SetNextWindowFocus();
            base.ImGUILayout();
        }

        protected override void ImGUIContent()
        {
            ImGui.InputText("Tileset name", ref tilesetName, 20);

            if (ImGui.Button("Cancel"))
            {
                cancelCallback?.Invoke(currentTileset);
                Reset();
                return;
            }
            ImGui.SameLine();
            if (ImGui.Button("Validate") && !string.IsNullOrWhiteSpace(tilesetName))
            {
                validateCallback?.Invoke(tilesetName, currentTileset);
                Reset();
                return;
            }

            ImGui.Image(currentTileset.RefTexture, new Vector2(currentTileset.tileTexture2D.Width, currentTileset.tileTexture2D.Height));
        }

        public void Open(Tileset selectedTileset, Action<string, Tileset> validate, Action<Tileset> cancel)
        {
            currentTileset = selectedTileset;
            tilesetName = selectedTileset.Name;
            validateCallback = validate;
            cancelCallback = cancel;
            isOpen = true;
        }

        public void UnsetTileset()
        {
            currentTileset = null;
        }

        void Reset()
        {
            currentTileset = null;
            validateCallback = null;
            cancelCallback = null;
            tilesetName = "";
            isOpen = false;
        }
    }
}