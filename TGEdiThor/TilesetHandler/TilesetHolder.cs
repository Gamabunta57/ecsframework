﻿using System.Collections.Generic;
using System.IO;

namespace TGEdiThor.TilesetHandler
{
    public class TilesetHolder
    {
        readonly string basePath;
        ImGuiRenderer editorRenderer;

        public Dictionary<string, Tileset> tilesetDictionary { get; private set; }

        public TilesetHolder(string basePath, ImGuiRenderer editorRenderer)
        {
            this.basePath = basePath;
            this.editorRenderer = editorRenderer;
            InitTileset();
        }

        public Tileset Add(string filePath)
        {
            if (tilesetDictionary.ContainsKey(filePath))
            {
                var tmpTileset = tilesetDictionary[filePath];
                editorRenderer.UnbindTexture(tmpTileset.RefTexture);
                tilesetDictionary.Remove(filePath);
            }

            var info = new FileInfo(filePath);
            var tileset = new Tileset(info);
            tileset.SetTexturePointer(editorRenderer.BindTexture(tileset.tileTexture2D));
            tilesetDictionary.Add(Path.GetFileNameWithoutExtension(filePath), tileset);
            return tileset;
        }

        public void Remove(Tileset tileset)
        {
            RemoveDictionaryKeyFor(tileset);
            editorRenderer.UnbindTexture(tileset.RefTexture);
        }

        public void Renamekey(string newName, Tileset tileset)
        {
            RemoveDictionaryKeyFor(tileset);
            tilesetDictionary.Add(newName, tileset);
        }

        public void SaveTileset(string tilesetName, Tileset tileset)
        {
            var filepath = Path.Combine(basePath, $"{tilesetName}.png");
            using (var file = new FileStream(filepath, FileMode.OpenOrCreate))
            {
                tileset.tileTexture2D.SaveAsPng(file, tileset.tileTexture2D.Width, tileset.tileTexture2D.Height);
                tileset.ResetPath(filepath);
            }
        }

        void RemoveDictionaryKeyFor(Tileset tileset)
        {
            var keyList = tilesetDictionary.Keys;
            string keyName = null;
            foreach (var kvp in tilesetDictionary)
            {
                if (kvp.Value == tileset)
                {
                    keyName = kvp.Key;
                    break;
                }
            }

            if (null != keyName)
            {
                tilesetDictionary.Remove(keyName);
            }
        }

        void InitTileset()
        {
            var dirInfo = new DirectoryInfo(basePath);
            var fileList = dirInfo.GetFiles("*.png");
            tilesetDictionary = new Dictionary<string, Tileset>(fileList.Length);

            foreach (var file in fileList)
            {
                Add(file.FullName);
            }
        }
    }
}