﻿using System;
using System.Drawing;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace TGEdiThor.TilesetHandler
{
    public class Tileset
    {
        public const int CellSize = 32;

        public Texture2D tileTexture2D;
        public IntPtr RefTexture { get; private set; }

        string filePath;

        public string Name
        {
            get; private set;
        }

        public Tileset(FileInfo textureFile)
        {
            filePath = textureFile.FullName;
            Name = Path.GetFileNameWithoutExtension(filePath);
            using (var stream = new MemoryStream())
            using (var bmp = Image.FromFile(textureFile.FullName))
            {
                bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                tileTexture2D = Texture2D.FromStream(TGEdiThorEntryPoint.GraphicsDeviceRef, stream);
            }
        }

        public void Remove()
        {
            tileTexture2D.Dispose();
            if(File.Exists(filePath))
                File.Delete(filePath);
        }

        public void SetTexturePointer(IntPtr refTexture)
        {
            RefTexture = refTexture;
        }

        public void ResetPath(string filePath)
        {
            this.filePath = filePath;
        }

        public void UnlinkPath()
        {
            filePath = null;
        }
    }
}