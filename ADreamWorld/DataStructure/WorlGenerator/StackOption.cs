﻿namespace ADreamWorld.WorldGenerator.DataStructure
{
    public enum StackOption
    {
        FullStackOnly,
        MixedStack,
        SubStackOnly
    }
}