﻿using System.Collections.Generic;

namespace ADreamWorld.WorldGenerator.DataStructure
{
    public struct GeneratorStep
    {
        public int RoomCountToGenerate;
        public int RoomCountToGenerateBis;

        public int CurrentAbilityIndex;
        public Ability CurrentAbility
        {
            get { return (Ability)CurrentAbilityIndex; }
            set { CurrentAbilityIndex = (int)value; }
        }

        public int StackOptionIndex;
        public StackOption StackOption
        {
            get { return (StackOption)StackOptionIndex; }
            set { StackOptionIndex = (int)value; }
        }

        public float ProbabilityToUseFullStack;

        public List<Room> TemplateRoomList;

        //public Ability Ability => (Ability)CurrentAbility;
        //public StackOption StackOptionSelected => (StackOption)StackOption;

        public static string[] AvailableStackOption => new string[]
        {
            StackOption.FullStackOnly.ToString(),
            StackOption.MixedStack.ToString(),
            StackOption.SubStackOnly.ToString()
        };

        public static string[] AvailableAbility => new string[]
        {
            Ability.None.ToString(),
            Ability.FirstAbility.ToString(),
            Ability.SecondAbility.ToString(),
            Ability.ThirdAbility.ToString(),
            Ability.FourthAbility.ToString(),
            Ability.FifthAbility.ToString()
        };
    }
}