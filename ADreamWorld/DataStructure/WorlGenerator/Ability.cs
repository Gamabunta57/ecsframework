﻿namespace ADreamWorld.WorldGenerator.DataStructure
{
    public enum Ability
    {
        None,
        FirstAbility,
        SecondAbility,
        ThirdAbility,
        FourthAbility,
        FifthAbility
    }
}