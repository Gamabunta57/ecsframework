﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ADreamWorld.WorldGenerator.DataStructure
{
    public struct Room
    {
        public int GridWidth;
        public int GridHeight;
        public List<Point> RoomSpaceList;
        public List<Point> HookList;
    }
}