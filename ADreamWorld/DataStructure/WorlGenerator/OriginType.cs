﻿namespace ADreamWorld.WorldGenerator.DataStructure
{
    public enum OriginType
    {
        Center,
        Random,
        Mix
    }
}