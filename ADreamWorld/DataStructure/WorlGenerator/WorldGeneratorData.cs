﻿using System.Collections.Generic;

namespace ADreamWorld.WorldGenerator.DataStructure
{
    public class WorldGeneratorData
    {
        //RNG
        public bool UseRandomSeed;
        public int Seed;

        //GRID
        public int GridWidth;
        public int GridHeight;

        public int OriginTypeIndex;
        public OriginType OriginType
        {
            get { return (OriginType)OriginTypeIndex; }
            set { OriginTypeIndex = (int)value; }
        }

        public List<GeneratorStep> StepList;

        public static string[] AvailableOrigins => new string[]
            {OriginType.Random.ToString(), OriginType.Center.ToString(), OriginType.Mix.ToString()};
    }

}