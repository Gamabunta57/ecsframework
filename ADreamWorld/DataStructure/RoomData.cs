﻿namespace ADreamWorld.DataStructure
{
    public class RoomData
    {
        public Tileset Tileset;
        public Tile[] TileList;
    }
}