﻿namespace ADreamWorld.ECS.ComponentKeys
{
    public static class ComponentKeyMetaComponent
    {
        public const int MovableComponent = 1;
        public const int PositionComponent = 1 << 1;
        public const int SpriteComponent = 1 << 2;
        public const int AnimateSpriteComponent = 1 << 3;
        public const int GravityComponent = 1 << 4;
        public const int InputComponent = 1 << 5;
        public const int JumpComponent = 1 << 6;
        public const int CollidingScreenComponent = 1 << 7;

    }
}
