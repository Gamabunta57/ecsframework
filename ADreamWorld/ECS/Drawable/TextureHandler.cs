﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ADreamWorld.ECS.Drawable
{
    static class TextureHandler
    {
        public static Texture2D BasicTexture { get; private set; }

        public static void LoadContent(ContentManager Content)
        {
            BasicTexture = Content.Load<Texture2D>("campfire");
        }
    }
}
