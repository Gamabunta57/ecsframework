﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class AnimateSpriteSystem : BaseUpdatableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.AnimateSpriteComponent |
            ComponentKeyMetaComponent.SpriteComponent;

        public AnimateSpriteSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        public override void Update(GameTime gameTime)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);

                meta.AnimateSpriteComponent.CurrentFrameDuration += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (meta.AnimateSpriteComponent.CurrentFrameDuration < meta.AnimateSpriteComponent.FrameDuration)
                {
                    continue;
                }

                meta.AnimateSpriteComponent.CurrentFrameDuration -= meta.AnimateSpriteComponent.FrameDuration;
                meta.SpriteComponent.TextureDestination.X += meta.AnimateSpriteComponent.SpriteWidth;

                if (meta.SpriteComponent.TextureDestination.X >= meta.AnimateSpriteComponent.SpriteColumn * meta.AnimateSpriteComponent.SpriteWidth)
                {
                    meta.SpriteComponent.TextureDestination.X = 0;
                }
            }
        }


    }
}
