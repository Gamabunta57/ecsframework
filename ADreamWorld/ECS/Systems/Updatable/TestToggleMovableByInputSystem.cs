﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class TestToggleMovableByInputSystem : BaseUpdatableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.MovableComponent;

        public TestToggleMovableByInputSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler)
        {
            ComponentHandler.DisableComponentEvent -= OnDisableComponent;
        }

        public override void Update(GameTime gameTime)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                var entity = entityList[i];
                ref var meta = ref ComponentHandler.GetComponent(entity.Id);
                var gamePad = GamePad.GetState(PlayerIndex.One);

                if (gamePad.IsButtonDown(Buttons.A))
                {
                    ComponentHandler.DisableComponent<MovableComponent>(ref entity);
                }

                if (gamePad.IsButtonDown(Buttons.B))
                {
                    ComponentHandler.EnableComponent<MovableComponent>(ref entity);
                }
            }
        }
    }
}
