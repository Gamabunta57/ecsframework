﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class MoveObjectSystem : BaseUpdatableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.PositionComponent |
            ComponentKeyMetaComponent.MovableComponent;

        public MoveObjectSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        public override void Update(GameTime gameTime)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);

                meta.PositionComponent.X += meta.MovableComponent.MoveIntent.X;
                meta.PositionComponent.Y += meta.MovableComponent.MoveIntent.Y;

                meta.MovableComponent.MoveIntent.X = meta.MovableComponent.MoveIntent.Y = 0;
            }
        }


    }
}
