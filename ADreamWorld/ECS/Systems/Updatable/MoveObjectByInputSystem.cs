﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class MoveObjectByInputSystem : BaseUpdatableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.MovableComponent |
            ComponentKeyMetaComponent.JumpComponent |
            ComponentKeyMetaComponent.InputComponent;

        public MoveObjectByInputSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);
                var gamePad = GamePad.GetState(meta.InputComponent.PlayerIndex);

                gamePad.ThumbSticks.Left.Normalize();

                meta.MovableComponent.MoveIntent.X = gamePad.ThumbSticks.Left.X * meta.MovableComponent.Speed * elapsed;

                if (meta.JumpComponent.IsJumping)
                {
                    //meta.JumpComponent.JumpCurrentDuration -= elapsed;
                    meta.MovableComponent.MoveIntent.Y -= (meta.JumpComponent.JumpForce + (gamePad.IsButtonDown(Buttons.A) ? meta.JumpComponent.JumpHeldForce : 0)) * elapsed;
                    //if (meta.JumpComponent.JumpCurrentDuration <= 0)
                    //meta.JumpComponent.IsJumping = false;

                }
                else if (gamePad.IsButtonDown(Buttons.A) && meta.JumpComponent.CanJump)
                {
                    meta.JumpComponent.CanJump = false;
                    meta.JumpComponent.IsJumping = true;
                    //meta.JumpComponent.JumpCurrentDuration = meta.JumpComponent.JumpTotalDuration;
                    meta.MovableComponent.MoveIntent.Y -= (meta.JumpComponent.JumpForce + meta.JumpComponent.JumpHeldForce) * elapsed;
                }


            }
        }


    }
}
