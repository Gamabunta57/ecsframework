﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class CollidingScreenSystem : BaseUpdatableSystem<MetaComponent>
    {

        public int ScreenWidth;
        public int ScreenHeight;

        public CollidingScreenSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.MovableComponent |
            ComponentKeyMetaComponent.SpriteComponent |
            ComponentKeyMetaComponent.JumpComponent |
            ComponentKeyMetaComponent.GravityComponent;

        public override void Update(GameTime gameTime)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);

                if (meta.PositionComponent.X < 0)
                {
                    meta.PositionComponent.X = 0;
                }
                else if (meta.PositionComponent.X + meta.SpriteComponent.TextureDestination.Width > ScreenWidth)
                {
                    meta.PositionComponent.X = ScreenWidth - meta.SpriteComponent.TextureDestination.Width;
                }

                if (meta.PositionComponent.Y + meta.SpriteComponent.TextureDestination.Height > ScreenHeight)
                {
                    meta.PositionComponent.Y = ScreenHeight - meta.SpriteComponent.TextureDestination.Height;
                    meta.GravityComponent.lastFallDownSpeedApplied = 0;
                    meta.JumpComponent.CanJump = true;
                    meta.JumpComponent.IsJumping = false;
                }
            }
        }

    }
}