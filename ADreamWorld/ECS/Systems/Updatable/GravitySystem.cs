﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Updatable
{
    class GravitySystem : BaseUpdatableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.MovableComponent |
            ComponentKeyMetaComponent.GravityComponent;

        public GravitySystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        public override void Update(GameTime gameTime)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);
                var moveApplied = (float)((meta.GravityComponent.lastFallDownSpeedApplied + GravityComponent.Gravity) * gameTime.ElapsedGameTime.TotalSeconds);

                meta.MovableComponent.MoveIntent.Y += moveApplied;
                meta.GravityComponent.lastFallDownSpeedApplied += moveApplied;
            }
        }

    }
}
