﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Triiinity.ECS.Components;
using Triiinity.ECS.System;

namespace ADreamWorld.ECS.Systems.Drawable
{
    class DisplaySpriteSystem : BaseDrawableSystem<MetaComponent>
    {
        protected override long GetComponentKey() =>
            ComponentKeyMetaComponent.PositionComponent |
            ComponentKeyMetaComponent.SpriteComponent;

        public DisplaySpriteSystem(ComponentHandler<MetaComponent> cmpHandler) : base(cmpHandler) { }

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (var i = 0; i < entityList.Count; i++)
            {
                ref var meta = ref ComponentHandler.GetComponent(entityList[i].Id);
                spriteBatch.Draw(meta.SpriteComponent.Texture, meta.PositionComponent.Position, meta.SpriteComponent.TextureDestination, Color.White);
            }
        }


    }
}
