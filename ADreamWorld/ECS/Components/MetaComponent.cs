﻿namespace ADreamWorld.ECS.Components
{
    public struct MetaComponent
    {
        public PositionComponent PositionComponent;
        public SpriteComponent SpriteComponent;
        public MovableComponent MovableComponent;
        public GravityComponent GravityComponent;
        public InputComponent InputComponent;
        public AnimateSpriteComponent AnimateSpriteComponent;
        public JumpComponent JumpComponent;
        public CollidingScreenComponent CollidingScreenComponent;
    }
}
