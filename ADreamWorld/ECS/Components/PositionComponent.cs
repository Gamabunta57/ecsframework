using Microsoft.Xna.Framework;

namespace ADreamWorld.ECS.Components
{
    public struct PositionComponent
    {
        public float X;
        public float Y;

        public Vector2 Position => new Vector2(X, Y);
    }
}