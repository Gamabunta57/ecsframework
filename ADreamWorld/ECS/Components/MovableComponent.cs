using Microsoft.Xna.Framework;

namespace ADreamWorld.ECS.Components
{
    public struct MovableComponent
    {
        public Vector2 MoveIntent;
        public float Speed;
    }
}