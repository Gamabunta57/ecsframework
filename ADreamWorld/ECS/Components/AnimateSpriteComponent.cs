
namespace ADreamWorld.ECS.Components
{
    public struct AnimateSpriteComponent
    {
        public float FrameDuration;
        public int SpriteWidth;
        public int SpriteHeight;
        public int SpriteColumn;
        public float CurrentFrameDuration;
    }
}