﻿namespace ADreamWorld.ECS.Components
{
    public struct JumpComponent
    {
        public float JumpForce;
        public float JumpHeldForce;
        public float JumpTotalDuration;
        public float JumpCurrentDuration;
        public bool CanJump;
        public bool IsJumping;
    }
}