﻿namespace ADreamWorld.ECS.Components
{
    public struct GravityComponent
    {
        public const float Gravity = 9.81f * 50;
        public float lastFallDownSpeedApplied;
    }
}
