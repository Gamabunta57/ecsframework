using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ADreamWorld.ECS.Components
{
    public struct SpriteComponent
    {
        public Texture2D Texture;
        public Rectangle TextureDestination;
    }
}