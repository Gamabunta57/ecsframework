﻿using ADreamWorld.ECS.ComponentKeys;
using ADreamWorld.ECS.Components;
using ADreamWorld.ECS.Drawable;
using ADreamWorld.ECS.Systems.Drawable;
using ADreamWorld.ECS.Systems.Updatable;
using ADreamWorld.WorldGenerator.DataStructure;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;
using Triiinity.ECS.Components;
using Triiinity.ECS.Entities;
using Triiinity.ECS.System;

namespace ADreamWorld
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class ADreamWorldGame : Game
    {
        protected readonly GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int virtualWidth = 800;
        const int virtualHeight = 480;
        const float ratio = (float)virtualHeight / virtualWidth;
        bool renderInRenderTargetOnly = false;
        Point virtualScreenSize = new Point() { X = virtualWidth, Y = virtualHeight };
        Rectangle _renderTargetDestination = new Rectangle(0, 0, virtualWidth, virtualHeight);
        public RenderTarget2D RenderTarget { get; private set; }

        Stopwatch counter = new Stopwatch();
        double benchTotalTime;
        int countMeasure;

        readonly BaseUpdatableSystem<MetaComponent>[] UpdatableSystems = new BaseUpdatableSystem<MetaComponent>[5];
        internal BaseDrawableSystem<MetaComponent>[] DrawableSystems { get; } = new BaseDrawableSystem<MetaComponent>[1];
        ComponentHandler<MetaComponent> cmpHandler = new ComponentHandler<MetaComponent>(typeof(ComponentKeyMetaComponent));

        WorldGeneratorData generatorData;

        public ADreamWorldGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            RenderTarget = new RenderTarget2D(GraphicsDevice, virtualWidth, virtualHeight, false, GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);


            int i = 0;
            UpdatableSystems[i++] = new AnimateSpriteSystem(cmpHandler);
            UpdatableSystems[i++] = new GravitySystem(cmpHandler);
            UpdatableSystems[i++] = new MoveObjectByInputSystem(cmpHandler);
            UpdatableSystems[i++] = new MoveObjectSystem(cmpHandler);
            UpdatableSystems[i++] = new CollidingScreenSystem(cmpHandler)
            {
                ScreenHeight = virtualHeight,
                ScreenWidth = virtualWidth
            };

            DrawableSystems[0] = new DisplaySpriteSystem(cmpHandler);
            base.Initialize();

            //windowHolder.SetWindowSize(Window.ClientBounds.Width, Window.ClientBounds.Height);
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += OnWindowSize;
            ResetTargetDestinationRectangle();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            var entityNumber = 1;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            TextureHandler.LoadContent(Content);
            cmpHandler.Init(entityNumber); 

            for (int i = 0; i < entityNumber - 1; i++)
            {
                var player = EntityHandler.GenerateEntity();
                ref MetaComponent metaComponent = ref cmpHandler.GetComponent(ref player);
                metaComponent.PositionComponent.X = i;
                metaComponent.PositionComponent.Y = 0;

                metaComponent.SpriteComponent.Texture = TextureHandler.BasicTexture;
                metaComponent.SpriteComponent.TextureDestination = new Rectangle(0, 0, 64, 64);
                metaComponent.MovableComponent.Speed = 25;

                cmpHandler.EnableMultipleComponent(ref player,
                    typeof(PositionComponent),
                    typeof(SpriteComponent),
                    typeof(MovableComponent),
                    typeof(GravityComponent)
                );
            }

            var player2 = EntityHandler.GenerateEntity();

            ref MetaComponent metaComponent2 = ref cmpHandler.GetComponent(ref player2);
            metaComponent2.PositionComponent.X = 0;
            metaComponent2.PositionComponent.Y = 260;

            metaComponent2.SpriteComponent.Texture = TextureHandler.BasicTexture;
            metaComponent2.SpriteComponent.TextureDestination = new Rectangle(0, 0, 64, 64);

            metaComponent2.AnimateSpriteComponent.SpriteColumn = 4;
            metaComponent2.AnimateSpriteComponent.CurrentFrameDuration = 0;
            metaComponent2.AnimateSpriteComponent.FrameDuration = .12f;
            metaComponent2.AnimateSpriteComponent.SpriteHeight = 64;
            metaComponent2.AnimateSpriteComponent.SpriteWidth = 64;

            metaComponent2.MovableComponent.Speed = 500;

            metaComponent2.InputComponent.PlayerIndex = PlayerIndex.One;

            metaComponent2.JumpComponent.CanJump = false;
            metaComponent2.JumpComponent.JumpForce = 1250;
            metaComponent2.JumpComponent.JumpHeldForce = 00;
            metaComponent2.JumpComponent.IsJumping = false;
            metaComponent2.JumpComponent.JumpTotalDuration = 0.0f;

            cmpHandler.EnableMultipleComponent(ref player2,
                typeof(PositionComponent),
                typeof(SpriteComponent),
                typeof(AnimateSpriteComponent),
                typeof(MovableComponent),
                typeof(InputComponent),
                typeof(GravityComponent),
                typeof(JumpComponent),
                typeof(CollidingScreenComponent)
            );

            // TODO: use this.Content to load your game content here
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            Console.WriteLine("Time : ");
            Console.WriteLine(benchTotalTime / countMeasure);
            Console.WriteLine(1000 / (benchTotalTime / countMeasure) + " FPS");
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            counter.Restart();
            foreach (var system in UpdatableSystems)
            {
                system.Update(gameTime);
            }
            benchTotalTime += counter.Elapsed.TotalMilliseconds;

            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            DrawToRenderTarget(gameTime);

            if (!renderInRenderTargetOnly)
            {
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                    SamplerState.PointClamp, DepthStencilState.Default,
                    RasterizerState.CullNone);
                GraphicsDevice.Clear(Color.Black);
                spriteBatch.Draw(RenderTarget, _renderTargetDestination, Color.White);
                spriteBatch.End();
            }

            counter.Stop();
            countMeasure++;
            base.Draw(gameTime);
        }

        public void SetNoRenderingInWindow()
        {
            renderInRenderTargetOnly = true;
        }

        void DrawToRenderTarget(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(RenderTarget);

            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.MediumPurple);

            foreach (var system in DrawableSystems)
            {
                system.Draw(spriteBatch);
            }

            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }

        void OnWindowSize(object sender, EventArgs args)
        {
            graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
            graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
            graphics.ApplyChanges();
            ResetTargetDestinationRectangle();
        }

        void ResetTargetDestinationRectangle()
        {
            int width = Window.ClientBounds.Width;
            int height = Window.ClientBounds.Height;

            if (width < height)
            {
                height = (int)(width * ratio);

                if (height > Window.ClientBounds.Height)
                {
                    height = Window.ClientBounds.Height;
                    width = (int)(height / ratio);
                }
            }
            else
            {
                width = (int)(height / ratio);

                if (width > Window.ClientBounds.Width)
                {
                    width = Window.ClientBounds.Width;
                    height = (int)(width * ratio);
                }
            }

            _renderTargetDestination.Width = width;
            _renderTargetDestination.Height = height;
            _renderTargetDestination.X = (int)((Window.ClientBounds.Width - width) * .5f);
            _renderTargetDestination.Y = (int)((Window.ClientBounds.Height - height) * .5f);
        }
    }
}
