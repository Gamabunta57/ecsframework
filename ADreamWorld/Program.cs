﻿using System;

namespace ADreamWorld
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new ADreamWorldGame())
                game.Run();
        }
    }
}
