@echo OFF

set targetfolder=ECSFRameworkV2\FrameworkImplementation\Components\*
set sourcefolder=ECSFRameworkV2\FrameworkImplementation\ComponentKeys\
set currentFolder=
set componentIds=

for /f "delims=" %%i  in ('dir /a:d /b %targetfolder%') do call :search_subfolder %%i
GOTO :EOF

:search_subfolder
    @echo on
    echo %1
    @echo off
    set currentFolder=%1
    for /f "delims=" %%i  in ('dir /a /b %targetfolder%\%currentFolder%') do call :set_component_id %%i
    GOTO :EOF
GOTO :EOF

:set_component_id
    set componentIds=%componentIds%;%1:~0,-3%
    @echo on
    echo componentIds
    @echo off
GOTO :EOF